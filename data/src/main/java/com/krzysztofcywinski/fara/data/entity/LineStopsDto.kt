package com.krzysztofcywinski.fara.data.entity

import com.squareup.moshi.Json

class LineStopsDto {

    @Json(name = "stops")
    var stops: List<StopDto>? = null

}
