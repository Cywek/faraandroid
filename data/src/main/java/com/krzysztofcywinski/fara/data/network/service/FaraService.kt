package com.krzysztofcywinski.fara.data.network.service

import com.krzysztofcywinski.fara.data.entity.LineStopsDto

import io.reactivex.Observable
import retrofit2.http.GET

interface FaraService {

    @get:GET("S3QwgEbf")
    val stops: Observable<LineStopsDto>

}
