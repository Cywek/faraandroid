package com.krzysztofcywinski.fara.data.entity

import com.squareup.moshi.Json

class TimeTableElementDto {

    @Json(name = "values")
    var values: List<TimeTableElementPairDto>? = null

}
