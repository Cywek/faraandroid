package com.krzysztofcywinski.fara.presentation.timetable.di

import android.content.Context
import com.krzysztofcywinski.fara.domain.repository.ZtmRepository
import com.krzysztofcywinski.fara.navigation.Navigator
import com.krzysztofcywinski.fara.presentation.application.di.ActivityScope
import com.krzysztofcywinski.fara.presentation.timetable.TimeTableActivity
import com.krzysztofcywinski.fara.presentation.timetable.TimeTablePresenter
import com.wdh.remotecontrol.presentation.base.schedulers.AndroidSchedulersProvider
import com.wdh.remotecontrol.presentation.base.schedulers.SchedulersProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class TimeTableActivityModule {

    @ActivityScope
    @Binds
    abstract fun bindsContext(activity: TimeTableActivity): Context

    @Module
    companion object {

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesNavigator(activity: TimeTableActivity): Navigator = Navigator(activity)

        @JvmStatic
        fun providesSchedulersProvider(): SchedulersProvider = AndroidSchedulersProvider()

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesTimeTablePresenter(view: TimeTableActivity, ztmRepository: ZtmRepository,
                                       navigator: Navigator
        ): TimeTablePresenter = TimeTablePresenter(view, ztmRepository, providesSchedulersProvider(), navigator)

    }
}