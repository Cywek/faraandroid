package com.krzysztofcywinski.fara.presentation.timetable;

import android.content.Context
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krzysztofcywinski.fara.R;
import com.krzysztofcywinski.fara.domain.model.TimeTableHourElement;
import kotlinx.android.synthetic.main.time_table_element.view.*
import java.lang.StringBuilder

class TimeTableAdapter(val items: List<TimeTableHourElement>, val context: Context)
    : RecyclerView.Adapter<TimeTableAdapter.TimeTableViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TimeTableViewHolder {
        return TimeTableViewHolder(LayoutInflater.from(context).inflate(R.layout.time_table_element, parent, false))
    }

    override fun onBindViewHolder(holder: TimeTableViewHolder?, position: Int) {
        holder?.hour?.text = items.get(position).hour.toString()
        val minutes = StringBuilder()
        for (minute in items.get(position).minutes) {
            minutes.append(String.format("%02d", minute))
            minutes.append("    ")
        }
        holder?.minute?.text = minutes.toString()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class TimeTableViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val hour = view.hour
        val minute = view.minute
    }
}
