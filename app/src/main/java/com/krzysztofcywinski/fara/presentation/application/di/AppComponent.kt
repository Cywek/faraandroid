package com.krzysztofcywinski.fara.presentation.application.di

import android.app.Application
import com.krzysztofcywinski.fara.presentation.application.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@AppScope
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            AppModule::class,
            BuildersModule::class
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}