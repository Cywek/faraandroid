package com.krzysztofcywinski.fara.presentation.timetable

import android.os.Bundle
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.domain.repository.ZtmRepository
import com.krzysztofcywinski.fara.mvp.BasePresenter
import com.krzysztofcywinski.fara.navigation.Navigator
import com.wdh.remotecontrol.presentation.base.schedulers.SchedulersProvider
import javax.inject.Inject

class TimeTablePresenter @Inject constructor(
        private val view: TimeTableActivity,
        private val ztmRepository: ZtmRepository,
        private val schedulersProvider: SchedulersProvider,
        private val navigator: Navigator
) : BasePresenter() {

    override fun bind(arguments: Bundle?, savedInstanceState: Bundle?) {
        super.bind(arguments, savedInstanceState)
        val stop = arguments!!["Stop"] as Stop
        view.setHeader(stop.line!!, stop.name!!, stop.direction!!)
        //TODO: show progress dialog?
        ztmRepository.getTimeTableHourElements(stop.stopId!!, stop.stopNr!!, stop.line!!)
                .subscribeOn(schedulersProvider.background())
                .observeOn(schedulersProvider.ui())
                .subscribe({
                    view.showTimeTable(it)
                }, {
                    view.showError(it.message)
                }).register()
    }

    fun onBackPressed() {
        navigator.goToMap()
    }
}