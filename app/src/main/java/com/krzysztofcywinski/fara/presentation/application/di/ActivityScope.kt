package com.krzysztofcywinski.fara.presentation.application.di

import javax.inject.Scope

@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class ActivityScope