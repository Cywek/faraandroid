package com.krzysztofcywinski.fara.presentation.map

import com.google.android.gms.maps.model.Marker
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.domain.model.map.MapModel
import com.krzysztofcywinski.fara.mvp.BasePresenter
import com.krzysztofcywinski.fara.navigation.Navigator
import javax.inject.Inject

class MapPresenter @Inject constructor(
        private val view: MapActivity,
        private val mapModel: MapModel,
        private val navigator: Navigator
) : BasePresenter() {

    private var stops: List<Stop>? = null

    fun onMapReady() {
        mapModel.closestStop.subscribe {
            view.setAllStopsDefaultColor()
            view.showClosestStop(it)
        }.register()
        mapModel.onLineStops.subscribe(
                {
                    stops = it.stops
                    for (stop in it.stops!!) {
                        view.printStop(stop)
                    }
                },
                {
                    view.showError(it.message)
                }
        ).register()
    }

    fun onLocationPermissionsGranted() {
        mapModel.startListeningForLocation()
    }

    fun onMarkerTextClicked(marker: Marker) {
        navigator.goToTimeTable(stops!!.filter { it.name.equals(marker.title) }.get(0))
    }
}