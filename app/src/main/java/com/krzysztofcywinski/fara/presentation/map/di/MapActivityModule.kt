package com.krzysztofcywinski.fara.presentation.map.di

import android.content.Context
import com.krzysztofcywinski.fara.domain.model.map.MapModel
import com.krzysztofcywinski.fara.domain.repository.LineStopsRepository
import com.krzysztofcywinski.fara.domain.repository.ZtmRepository
import com.krzysztofcywinski.fara.navigation.Navigator
import com.krzysztofcywinski.fara.presentation.application.di.ActivityScope
import com.krzysztofcywinski.fara.presentation.map.MapActivity
import com.krzysztofcywinski.fara.presentation.map.MapPresenter
import com.krzysztofcywinski.fara.repository.LocationRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class MapActivityModule {

    @ActivityScope
    @Binds
    abstract fun bindsContext(activity: MapActivity): Context

    @Module
    companion object {

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesNavigator(activity: MapActivity): Navigator = Navigator(activity)

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesModel(lineStopsRepository: LineStopsRepository, activity: MapActivity, ztmRepository: ZtmRepository
        ): MapModel = MapModel(lineStopsRepository, LocationRepository(activity), ztmRepository)

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesMapPresenter(view: MapActivity, model: MapModel, navigator: Navigator
        ): MapPresenter = MapPresenter(view, model, navigator)

    }
}