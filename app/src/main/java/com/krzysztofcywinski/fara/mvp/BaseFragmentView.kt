package com.krzysztofcywinski.fara.mvp

import android.content.Context
import android.support.v4.app.Fragment
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragmentView : Fragment() {

    abstract val presenter: BasePresenter

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        presenter.bind(arguments, null)
        onViewBound()
    }

    abstract fun onViewBound()

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        presenter.unbind()
        super.onDestroy()
    }
}