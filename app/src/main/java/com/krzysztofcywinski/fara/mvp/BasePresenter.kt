package com.krzysztofcywinski.fara.mvp

import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter {

    private val disposables = CompositeDisposable()

    private fun registerDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    open fun bind(arguments: Bundle?, savedInstanceState: Bundle?) {

    }

    open fun unbind() {
        disposables.dispose()
    }

    open fun onResume() {

    }

    open fun onPause() {

    }

    /**
     * Extension for registering disposables
     */
    protected fun Disposable.register() {
        val presenter = this@BasePresenter
        presenter.registerDisposable(this)
    }
}