package com.krzysztofcywinski.fara.presentation.timetable

import android.os.Bundle
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.domain.model.TimeTableHourElement
import com.krzysztofcywinski.fara.domain.repository.ZtmRepository
import com.krzysztofcywinski.fara.navigation.Navigator
import com.krzysztofcywinski.fara.presentation.base.schedulers.TestSchedulersProvider
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class TimeTablePresenterTest {

    private lateinit var presenter: TimeTablePresenter

    @Mock
    private lateinit var mockedView: TimeTableActivity

    @Mock
    private lateinit var ztmRepository: ZtmRepository

    @Mock
    private lateinit var mockedBundle: Bundle

    @Mock
    private lateinit var mockedNavigator: Navigator

    private val schedulersProvider: TestSchedulersProvider = TestSchedulersProvider()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = TimeTablePresenter(mockedView, ztmRepository, schedulersProvider, mockedNavigator)
    }

    @Test
    fun `Should start listening for location after location permissions is granted`() {
        // given
        val stop = Stop()
        stop.direction = "Banacha"
        stop.name = "Centrum 07"
        stop.stopNr = "07"
        stop.stopId = 1234L
        stop.lon = 12.22
        stop.lat = 22.22
        stop.line = "35"
        Mockito.`when`(mockedBundle["Stop"]).thenReturn(stop)
        Mockito.doNothing().`when`(mockedView).setHeader(stop.line!!, stop.name!!, stop.direction!!)
        val list = ArrayList<TimeTableHourElement>() as List<TimeTableHourElement>
        Mockito.`when`(ztmRepository.getTimeTableHourElements(stop.stopId!!, stop.stopNr!!, stop.line!!))
                .thenReturn(Observable.just(list))

        // when
        presenter.bind(mockedBundle, mockedBundle)

        // then
        verify(mockedView).showTimeTable(list)
    }

}