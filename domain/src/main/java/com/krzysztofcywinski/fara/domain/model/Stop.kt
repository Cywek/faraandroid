package com.krzysztofcywinski.fara.domain.model

import android.os.Parcel
import android.os.Parcelable

open class Stop() : Parcelable {

    var direction: String? = null

    var name: String? = null

    var lat: Double? = null

    var lon: Double? = null

    var line: String? = null

    var stopId: Long? = null

    var stopNr: String? = null

    constructor(parcel: Parcel) : this() {
        direction = parcel.readString()
        name = parcel.readString()
        lat = parcel.readValue(Double::class.java.classLoader) as? Double
        lon = parcel.readValue(Double::class.java.classLoader) as? Double
        line = parcel.readString()
        stopId = parcel.readValue(Long::class.java.classLoader) as? Long
        stopNr = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(direction)
        parcel.writeString(name)
        parcel.writeValue(lat)
        parcel.writeValue(lon)
        parcel.writeString(line)
        parcel.writeValue(stopId)
        parcel.writeString(stopNr)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Stop> {
        override fun createFromParcel(parcel: Parcel): Stop {
            return Stop(parcel)
        }

        override fun newArray(size: Int): Array<Stop?> {
            return arrayOfNulls(size)
        }
    }

}
