package com.wdh.remotecontrol.presentation.base.schedulers

import io.reactivex.Scheduler

interface SchedulersProvider {
    fun background(): Scheduler
    fun ui(): Scheduler
}