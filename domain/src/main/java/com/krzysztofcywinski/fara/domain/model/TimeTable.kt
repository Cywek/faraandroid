package com.krzysztofcywinski.fara.domain.model

import com.krzysztofcywinski.fara.data.entity.TimeTableElementDto
import com.krzysztofcywinski.fara.data.entity.TimeTableElementPairDto
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*

class TimeTable {

    var elements: List<TimeTableElement>? = null

    companion object {

        fun fromDto(timeTableList: List<TimeTableElementDto>): TimeTable {
            val elements = ArrayList<TimeTableElement>()
            val dtf: DateTimeFormatter? = DateTimeFormat.forPattern("HH:mm:ss")

            for (elementDto in timeTableList) {
                val element = TimeTableElement()
                for (pair: TimeTableElementPairDto in elementDto.values!!) {
                    if ("czas".equals(pair.key)) {
                        element.time = dtf!!.parseLocalTime(pair.value)
                    } else if ("kierunek".equals(pair.key)) {
                        element.direction = pair.value!!
                    }
                }
                elements.add(element)
            }

            val timeTable = TimeTable()
            timeTable.elements = elements
            return timeTable
        }
    }
}
