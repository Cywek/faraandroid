package com.krzysztofcywinski.fara.domain.source

import android.net.ConnectivityManager
import com.krzysztofcywinski.fara.data.network.provider.FaraServiceProvider
import com.krzysztofcywinski.fara.data.network.service.FaraService
import com.krzysztofcywinski.fara.domain.datasource.FaraDataSource
import com.krzysztofcywinski.fara.domain.model.LineStops
import io.reactivex.Observable
import java.io.File

class FaraDataSourceImpl(connectivityManager: ConnectivityManager,
                         cacheDir: File) : FaraDataSource {
    private val service: FaraService

    override val stops: Observable<LineStops>
        get() = service.stops.map {
            dto -> LineStops.fromDto(dto.stops!!)
        }

    init {
        service = FaraServiceProvider.newInstance(
                connectivityManager,
                cacheDir,
                API_BASE_URL)
                .get()
    }

    companion object {
        var API_BASE_URL = "https://pastebin.com/raw/"
    }
}
