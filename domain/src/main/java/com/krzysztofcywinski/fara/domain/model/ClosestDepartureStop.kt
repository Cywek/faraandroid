package com.krzysztofcywinski.fara.domain.model

import org.joda.time.LocalTime


class ClosestDepartureStop: Stop() {

    var departureTime: LocalTime? = null

}
