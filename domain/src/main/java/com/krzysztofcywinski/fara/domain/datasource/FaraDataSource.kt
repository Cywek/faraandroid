package com.krzysztofcywinski.fara.domain.datasource

import com.krzysztofcywinski.fara.domain.model.LineStops

import io.reactivex.Observable


interface FaraDataSource {

    val stops: Observable<LineStops>

}
