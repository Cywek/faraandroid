package com.krzysztofcywinski.fara.domain.model

import com.krzysztofcywinski.fara.data.entity.StopDto
import java.util.*

class LineStops {

    var stops: List<Stop>? = null

    companion object {

        fun fromDto(stopDtoList: List<StopDto>): LineStops {
            val stops = ArrayList<Stop>()

            for (stopDto in stopDtoList) {
                val stop = Stop()
                stop.direction = stopDto.direction
                stop.name = stopDto.name
                stop.lat = stopDto.lat
                stop.lon = stopDto.lon
                stop.line = stopDto.line
                stop.stopId = stopDto.stopId!!.substring(0,4).toLong()
                stop.stopNr = stopDto.stopId!!.substring(4,6)
                stops.add(stop)
            }

            val lineStops = LineStops()
            lineStops.stops = stops
            return lineStops
        }
    }
}
