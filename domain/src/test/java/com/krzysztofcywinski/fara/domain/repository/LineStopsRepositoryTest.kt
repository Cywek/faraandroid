package com.krzysztofcywinski.fara.domain.repository

import com.krzysztofcywinski.fara.domain.model.LineStops
import com.krzysztofcywinski.fara.domain.source.FaraDataSourceImpl
import com.krzysztofcywinski.fara.presentation.base.schedulers.TestSchedulersProvider
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LineStopsRepositoryTest {

    private lateinit var lineStopsRepository: LineStopsRepository

    @Mock
    private lateinit var mockedFaraDataSourceImpl: FaraDataSourceImpl

    private val schedulersProvider: TestSchedulersProvider = TestSchedulersProvider()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Should return line stops`() {
        // given
        //region json
        val json = "{\n" +
                "    \"stops\": [\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.230462,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 21.011332,\n" +
                "            \"name\": \"Centrum 07\",\n" +
                "            \"stopId\": \"701307\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.235629,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 21.008242,\n" +
                "            \"name\": \"Metro Świętokrzyska 05\",\n" +
                "            \"stopId\": \"701405\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.23862,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 21.006364,\n" +
                "            \"name\": \"Królewska 05\",\n" +
                "            \"stopId\": \"701505\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.243401,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 21.002454,\n" +
                "            \"name\": \"Pl. Bankowy 07\",\n" +
                "            \"stopId\": \"701607\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.244686,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 21.000922,\n" +
                "            \"name\": \"Metro Ratusz Arsenał 03\",\n" +
                "            \"stopId\": \"709903\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.249669,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.998733,\n" +
                "            \"name\": \"Muranów 05\",\n" +
                "            \"stopId\": \"701705\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.253606,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.997271,\n" +
                "            \"name\": \"Muranowska 11\",\n" +
                "            \"stopId\": \"701811\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.252688,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.990277,\n" +
                "            \"name\": \"Dzika 03\",\n" +
                "            \"stopId\": \"702003\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.251351,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.985354,\n" +
                "            \"name\": \"Stawki 05\",\n" +
                "            \"stopId\": \"708405\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.253872,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.983159,\n" +
                "            \"name\": \"Rondo 'Radosława' 05\",\n" +
                "            \"stopId\": \"709105\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.255416,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.982144,\n" +
                "            \"name\": \"Rondo 'Radosława' 09\",\n" +
                "            \"stopId\": \"709109\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.262406,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.978472,\n" +
                "            \"name\": \"Pl. Grunwaldzki 07\",\n" +
                "            \"stopId\": \"603907\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.26435,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.971461,\n" +
                "            \"name\": \"Sady Żoliborskie 03\",\n" +
                "            \"stopId\": \"604003\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.266736,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.962833,\n" +
                "            \"name\": \"Włościańska 03\",\n" +
                "            \"stopId\": \"604103\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.268011,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.958243,\n" +
                "            \"name\": \"Park Olszyna 03\",\n" +
                "            \"stopId\": \"604203\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.269392,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.953235,\n" +
                "            \"name\": \"Romaszewskiego 03\",\n" +
                "            \"stopId\": \"604303\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.271497,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.94564,\n" +
                "            \"name\": \"Piaski 03\",\n" +
                "            \"stopId\": \"604503\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.274998,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.937923,\n" +
                "            \"name\": \"Al. Reymonta 08\",\n" +
                "            \"stopId\": \"604608\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.270731,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.93375,\n" +
                "            \"name\": \"Conrada 06\",\n" +
                "            \"stopId\": \"604706\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.269069,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.932856,\n" +
                "            \"name\": \"Ogrody Działkowe Bemowo 05\",\n" +
                "            \"stopId\": \"506505\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.263103,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.927067,\n" +
                "            \"name\": \"Piastów Śląskich 05\",\n" +
                "            \"stopId\": \"506205\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"direction\": \"Banacha\",\n" +
                "            \"lat\": 52.260309,\n" +
                "            \"line\": \"35\",\n" +
                "            \"lon\": 20.924862,\n" +
                "            \"name\": \"Nowe Bemowo 09\",\n" +
                "            \"stopId\": \"516109\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        //endregion
        val moshi = Moshi.Builder().build();
        val jsonAdapter = moshi.adapter(LineStops::class.java)
        val lineStops = jsonAdapter.fromJson(json)
        Mockito.`when`(mockedFaraDataSourceImpl.stops)
                .thenReturn(Observable.just(lineStops))
        lineStopsRepository = LineStopsRepository(mockedFaraDataSourceImpl, schedulersProvider)

        // when
        lineStopsRepository.onLineStops

        // then
        val subscriber = lineStopsRepository.onLineStops.test()
        subscriber.assertValue(lineStops)
    }
}