package com.krzysztofcywinski.fara.presentation.base.schedulers

import com.wdh.remotecontrol.presentation.base.schedulers.SchedulersProvider
import io.reactivex.schedulers.Schedulers

class TestSchedulersProvider : SchedulersProvider {
    override fun background() = Schedulers.trampoline()
    override fun ui() = Schedulers.trampoline()
}